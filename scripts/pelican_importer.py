#!/usr/bin/env python3

import sys, os, time
import datetime
from os import path

import pypandoc

FM_TRANSLATION_KEY = {
    'status': 'draft',
    'subtitle': 'description'
}

FM_TRANSLATION_VALUE = {
    'status': lambda x: x != 'published' and 'true' or 'false',
    'date': lambda x: x.split()[0],
    'tags': lambda x: '[{}]'.format(', '.join(map(lambda y: y.strip(), x.split(",")))),
    'summary': lambda x: '"{}"'.format(x.endswith('.') and x[:-1] or x),
    'title': lambda x: '"{}"'.format(x)
}

if sys.stdin.isatty():
    print("That's an error, I guess...")
    sys.exit(255)

in_lines = sys.stdin.read().splitlines()

for in_line in in_lines:
    # paths
    phile = in_line
    output = path.splitext(phile)[0] + '.md'

    with open(phile) as f:
        read_lines = f.readlines()

    # divide et impera
    for i,line in enumerate(read_lines):
        if line == '\n':
            splitting = i
            break
    fm_content = read_lines[:splitting]
    mk_content = read_lines[splitting+1:]

    # converting front-matter
    ## preprocessing
    clean_fm = []
    date = None
    for line in fm_content:
        if line.startswith(":"):
            clean_fm.append(line)
        else:
            # continuation line
            last_line = clean_fm.pop().strip() + " "
            clean_fm.append(last_line + line.strip())

    front_matter = []
    for line in clean_fm:
        tokens = line.split(":", 2)
        key = tokens[1]
        value = tokens[2].strip()

        front_matter.append('{}: {}'.format(
            key in FM_TRANSLATION_KEY and FM_TRANSLATION_KEY[key] or key,
            (key in FM_TRANSLATION_VALUE and FM_TRANSLATION_VALUE[key](value) or value).replace('\\', '')
        ))

        # save date for later
        if key == 'date':
            date = value 
    front_matter = ["---"] + front_matter + ["---"]

    # converting content
    converted_content = pypandoc.convert_text(''.join(mk_content), 'gfm', format='rst', extra_args=['--atx-headers'])
    
    ## filter out some changes
    filtered_content = []
    prev_line = None
    for line in converted_content.split("\n"):
        if line == '' and prev_line == '':
            continue

        if "%7Bfilename%7D" in line:
            line = line.replace("%7Bfilename%7D", "")

        if "/articles" in line:
            line = line.replace("/articles", "/posts")

        filtered_content.append(line)
        prev_line = line

    # finally write converted file
    with open(output, 'w') as f:
        f.write('\n'.join(front_matter))
        f.write('\n')
        f.write('\n'.join(filtered_content))
    
    # update atime, mtime
    if date is not None:
        year = int(date[:4])
        month = int(date[5:7])
        day = int(date[8:10])
        hour = int(date[11:13])
        minute = int(date[14:16])
        second = 0
        mtime = time.mktime(datetime.datetime(
            year=year,
            month=month,
            day=day,
            hour=hour,
            minute=minute,
            second=second
        ).timetuple())
        os.utime(output, (mtime, mtime))