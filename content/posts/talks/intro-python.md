---
title: "Introduction to Python 3"
date: 2016-06-01
tags: [talk, python, python3, programming]
draft: false
summary: "A basic introduction to the Python language version 3"
---
<span class="left">
    ![python3 logo](/images/posts/python.png)
</span>

A language that is increasingly becoming more and more famous and
prominent, both as a scripting language and a scientifical tool, is
[Python](https://www.python.org/).

Python lets newcomers accomodate really quickly and easily. Discover it
yourself\!

[Here](/raw/intro-python/presentation.html) I provide a small
introduction to basic concepts of becoming a *pythonista*.

If you are interested you can download a
[zip](/files/linuxvar/intro_python/introduzione_python.zip) containing
the presentation.
