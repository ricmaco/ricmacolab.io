---
title: "Young People and SNSs"
date: 2015-10-26
tags: [young people, social network]
draft: false
summary: "How do young people use social network sites?"
---
In an Internet reality populated by real
[virtualities](https://en.wikipedia.org/wiki/Virtuality_\(philosophy\)),
in which young people is completely absorbed in everyday life too, it is
becoming more and more fundamental to discuss and understand how young
people (13-20) use these new technologies and how much understand of
their possible dangers.

I ideated a small questionnaire to gather some data about what personal
informations are usually provided to social network sites by young
people and how they interact with this huge amount of data.

The questionnaire was aired on October 24, 2015 at the [Linux Day 2015
of Varese](http://linuxvar.it/content/linux-day-2015) in the [ISISS
Antonio Sant'Elia](http://www.istitutosantelia.gov.it), an Italian high
school, featuring the students as questionee.

I took the opportunity and displayed the results in a small
presentation, where in the last slides I talked about Facebook T.O.S.
(**T**erms **o**f **S**ervice) as an example of social network site
licence agreement.

Slides can be found [here](/files/linuxday/2015/questionario.zip).

This, instead, is a chart of the results:

<span class="fullwidth">
    ![questionnary aggregations displayed as image](/images/posts/questionnaire.jpg)
</span>