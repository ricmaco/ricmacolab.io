---
title: "Introduction to Git"
date: 2015-05-18
tags: [talk, git, programming]
draft: false
summary: "Cheatsheet to a basic use of the most powerful and used versioning system of the world"
---
<span class="center">
    ![git cvs logo](/images/posts/git.png)
</span>

Git, THE content versioning system. Now it is a standard tool in the
swiss army knife of every programmer in the world, worthy of this name.
To the few, who are relatively new to programming and use other
inefficient systems to share and catalogue their code, it is essential
to fill the gap in their toolset.

Here I provided a (very) basic introduction to start using this
wonderful, essential tool.

For the sake of opennes both a [ODP
file](/files/linuxvar/intro_git/introduzione_git.odp) and [PDF
file](/files/linuxvar/intro_git/introduzione_git.pdf) is available.
