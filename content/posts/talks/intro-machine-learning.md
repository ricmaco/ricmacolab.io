---
title: "Introduction to Machine Learning"
date: 2018-03-23
tags: [2018, talk, introduction, machine learning]
draft: false
summary: "Introductory talk to machine learning concepts"
---
<span class="center background">
  ![perceptron schematics](/images/posts/perceptron.png)
</span>

As I stated in some article before, in the LUG I use to attend, we give
some talks to try to share as much knowledge as possible, over our
current interests.

I always try to join these events, because they offer me a unique
occasion to train my presentation skills and to write down some of the
concepts I am currently looking into.

In this time period, I just finished a university lecture over some
machine learning concepts. Not that I consider myself an expert in this
field, but I feel confident enough to explain the basics to completely
agnostic people.

So I dediced to give an introductory talk about this topic. The talk
night took place the 21<sup>st</sup> of March, 2018. The talk has been
recorded on video (more info later).

# Presentation

The presentation slides are of course in Italian and you can view them
below.

<div class="container-pdf">
  <object type="application/pdf" data="/files/linuxvar/intro_machine_learning/intro_machine_learning.pdf#zoom=page-width">
    <p>Error: PDF file cannot be displayed!</p>
  </object>
</div>

You can also download the presentation slide, in [ODP
format](/files/linuxvar/intro_machine_learning/intro_machine_learning.odp)
or in [PDF
format](/files/linuxvar/intro_machine_learning/intro_machine_learning.pdf).

# Video

The video recording of the night is available on YouTube, through the
[LinuxVar
channel](https://www.youtube.com/channel/UCoYyCUclcvKVnvyfgla4I0g). (I
know the caption title of the video is wrong, but the presentation is
the right one)

<div class="container-video">
  <iframe class="video" allowfullscreen="" frameborder="0"
    src="https://www.youtube.com/embed/ihQgyewHw9c">
  </iframe>
</div>

Of course, if you have any questions, you can write me using one of the
contact badges on the page aside.
