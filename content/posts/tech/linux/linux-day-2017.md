---
title: "Linux Day 2017"
date: 2017-10-28
tags: [2017, linuxday, linuxvar, linux, talks, instagram, privacy]
draft: false
summary: "2017 edition of the Linux Day for the province of Varese, Italy"
---
{{<figure
    src="/images/posts/linuxday2017.png"
    alt="linuxday logo 2017"
    class="left background"
>}}

The fourth saturady of october has come, which means it is time for
[Linux Day](http://www.linuxday.it). As always I and the group I am a
member of organized the edition for the area of Varese, Lombardy.

We gave force to the usual collaboration with other local associations,
such as us, that is
[LinuxVar](http://www.linuxvar.it),
[LIFO](http://lifolab.org) laboratory and
[GL-Como](http://www.gl-como.it/) LUG. For the third year the decision
on the location fell on the [FaberLab](http://www.faberlab.org) of
Tradate, a place very apt to organize this types of events, and home of
DIY hobbyists who like to tinker with 3D printing.

As every year some talks and some exhibitions of various proofs of
concept took place. The morning being reserved to presentations
regarding topics close to the new generations, such as Internet
security, decentralized social network sites and the powers of computer
science and open source.

# Me

This year I gave a simple talk about Instagram (and all other social
network sites) dangers. You can find more info about the presentation
[here](/posts/talks/instagram-instaprivacy).

# Resources

If you want to see some photos (mainly of the necks of the public) of
the event, you can find them on [GL-Como Friendica
page](https://social.gl-como.it/photos/gl-como).

The resources used for quite all talks are available on the [LinuxVar
site](https://www.linuxvar.it/linux-2017/).
