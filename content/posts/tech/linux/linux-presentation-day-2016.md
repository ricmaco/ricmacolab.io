---
title: "Linux Presentation Day 2016"
date: 2016-05-01
tags: [2016, linuxvar, linux, talks, presentation, firewall, traffic sniffing, appinventor]
draft: false
summary: "2016 (and first) edition of the Linux Presentation Day for our LUG, hosted at Gazzada, Varese"
---
{{<figure
    src="/images/posts/lpd-2016.png"
    alt="linux presentation day logo 2016"
    class="right background"
>}}

A new (but not so revolutionary) event is born in Europe to advertise
the use of Linux and the concept of Open Source and Free Software: the
Linux Presentation Day. All European nations estabilished a day, the
30th of April, to organize some propaganda for the famouse operative
system, I like the most.

My LUG, [LinuxVar](http://linuxvar.it/), has decided to take an active
part in this day, but in its own classic style.We decided to spend this
day in a school, teaching to the students the power of open systems. We
chose the [I.S.I.S Keynes](http://www.isiskeynes.it/) in [Gazzada
Schianno](http://www.openstreetmap.org/#map=19/45.77349/8.82783&layers=N),
Varese.

A group decided to present an acitivity based on
[AppInventor](http://appinventor.mit.edu/explore/), an MIT software
which allows to develop Android applications in a
[Scratch](https://scratch.mit.edu/) fashion, dealing with visual blocks
for coding.

Me and my friend decided to give an introductory talk about Internet
traffic sniffing, so we presented principal Linux networking tools
together with `nmap` and Wireshark. My friend centered his attention on
a presentation over firewalls tassonomy and firewalls tasks.

{{<pdf data="/files/linuxday/2016p/analisi-rete.pdf">}}

A copy of the presentation on traffic sniffing is available to download
[here](/files/linuxday/2016p/analisi-rete.pdf).
