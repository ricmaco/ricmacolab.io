---
title: "How did I run this site?"
date: 2015-04-22
tags: [site, hardware, python, static generator, embedded, rpi3, raspberry, pi, nginx]
draft: false
summary: "Hardware (and consequent software) which used to serve you this modest site"
---
<span class="right background">
    ![raspberry pi 3 photo](/images/posts/rpi3.jpg)
</span>

> Note that now the site is served through [Gitlab Pages](https://about.gitlab.com/product/pages/) and
> [Netlify](https://www.netlify.com/). I am still concerned about privacy, but I don't own a stable connection anymore,
> so...

I have never been a fan of giving away all my informations and
personally produced content to a service provider I don't completely
know and trust. I had a bad experience, when my virtual server provider
deleted all my files without my consent or any prior warning and,
remembering that, I actually prefer to handle my own data by myself.

In order to do that, I need some sort of association from an hostname to
my ISP provided dynamic IP. So I bought the domain you are seing on top,
mainly because its provider offered a dynamic DNS service included.
Before that, I used a third level free dynamic DNS service provided by
the [LUG](http://linuxvar.it/content/dynamic-dns) (Linux User Group) I
attend.

Alas the Internet connection in my country isn't that great, but I,
personally speaking, cannot complain at all. I have an asymetric DSL
12 / 0.8 Mbps, quite enough for offering simple services such as HTTP and
git. I connect to the Internet by means of a cheap router that supports
high speed ethernet and a WLAN.

But the real star of this page is the server phisically providing this
service, a [Raspberry
Pi 3](https://www.raspberrypi.org/products/raspberry-pi-3-model-b/),
produced by [Raspberry Foundation](https://www.raspberrypi.org/). It is
an embedded, single board, but modestly high powered computer, backed by
an [ARMv8](https://en.wikipedia.org/wiki/ARM_architecture#ARMv8-A)
processor and 1 GB of RAM, running my beloved [Arch
Linux](https://www.archlinux.org/), ARM version.

# Software

<span class="right background">
    ![nginx logo](/images/posts/nginx.png)
</span>

I chose [nginx](https://www.nginx.com/) as web server, because it is
very light in terms of used resources and easy to configure, although I
currently don't use any server side scripting engine, like PHP or
Python. I plan to set up some Python FCGI in order to provide some small
services.

The site is, therefore, static, but not completely handwritten. As
stated in the footer at the bottom of every page, this site is generated
with the excellent static content generator
[Pelican](http://getpelican.com/).

> Ehm... Update, the site has been now ported to [Hugo](https://gohugo.io).
