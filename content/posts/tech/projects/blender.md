---
title: "Simple scene in blender"
date: 2015-09-25
tags: [blender, cycles, render, wallpaper]
draft: false
summary: "Realization of a sample for showing the powers of Blender Cycles rendering engine"
---
<span class="right">
    ![blender logo](/images/posts/blender.png)
</span>

Blender is a 3D modeling tool which became famouse after its supporting
foundation, the [Blender
Foundation](http://www.blender.org/foundation/), produced some really
neat [3D animated
movies](http://archive.blender.org/features-gallery/movies/index.html),
the most famous being [Big Buck Bunny](http://www.bigbuckbunny.org).

Originally it shipped with a very imprecise renderer, with no basis on
physical laws and really, really inefficient. For this matter, it was
seen more as a toy, than a wannabe professional tool.

Then the Cycles engine appeared and all changed. It was the first open
source implmentation of a path tracer, ready to be modified to one's
needs. Some companies saw its potential and backed the project with
founds. Now the project is getting bigger and bigger and it is starting
to be used in a professional environment. The bidirectional path tracer
and the montecarlo extension are under development, so news are to be
expected.

I made a little test render, with an Egyptian scene, to provide a sample
of Cycles rendering engine powers. Here are the results (I know I am not
very good at it, but I'm still learning).

<span class="fullwidth background">
    ![first rendering image](/images/posts/blender-render1.jpg)
</span>

<span class="fullwidth background">
    ![second rendering image](/images/posts/blender-render2.jpg)
</span>

You can download the full resolution version of the
[first](/files/uni/blender/render-hi-1.png) and
[second](/files/uni/blender/render-hi-2.png) image, to use it as
wallpaper or anything you like, but caution, every image is 8 MB and I
serve the site on a slow connection...

My PC is only equipped with CPU rendering and, with a quality of 50 rays
for pixel, render has completed in about 3 hours and 20 minutes. If you
think you can do better here it is the [blend
file](/files/uni/blender/project.tar.gz).
