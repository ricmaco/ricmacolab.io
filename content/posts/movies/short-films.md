---
title: "Some short films"
date: 2015-04-18
tags: [short movies, stop motion, chroma key]
draft: false
description: Just a dump of some clips I produced for a multimedia project exam at university.
---
Ok, my directing, editing and producing ability is not at its finest,
although I'm learning. It is not my field, though, so I do not expect to
improve much.

I had to realize two clips: one using the stop motion technique and one
using a chroma key footage. Here they are:

<div class="container-video">
  <iframe class="video" src="https://www.youtube.com/embed/AKndch7vk-E"
    frameborder="0" allowfullscreen></iframe>
</div>

<div class="container-video">
  <iframe class="video" src="https://www.youtube.com/embed/nhOYaf9BSEU"
    frameborder="0" allowfullscreen></iframe>
</div>

Quite amatorial, right? Well, it's a start.
