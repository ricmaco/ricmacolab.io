---
title: "Projects"
tags: [projects, webapps]
draft: false
summary: "List of web projects currently hosted here"
---
* [Italian Wordslist Generator](https://ricmaco.gitlab.io/iws/)
