---
title: "Licenses"
category: info
tags: [license, creative commons, fonts]
draft: false
summary: "All licensing matters of this site"
---
This site and all its content are licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/), unless othewise specified (such as for the theme, which is used and provided under [MIT License](https://opensource.org/licenses/MIT)).

The rights for the site name, logo, favicon and images of the author (wherever found) are all reserved, together with all the material marked with a &copy;.

The fonts used are:

  - [Special Elite](http://www.fontsquirrel.com/fonts/special-elite/),
    by [Astigmatic](http://www.astigmatic.com/), licensed under the
    [Apache License, 2](https://www.apache.org/licenses/).
  - [Ruda](http://www.google.com/fonts/specimen/Ruda), by [Marie
    Monsalve](http://www.mukamonsalve.com.ar/) and [Angelina
    Sanchez](http://www.angelinasanchez.com.ar/), licensed under the
    [SIL Open Font License, 1.1](http://scripts.sil.org/OFL).

Images contained in this site, unless otherwise specified, belong to
their respective copyright owners and are therefore possibly used
under "fair use".
