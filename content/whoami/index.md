---
title: "Who am I?"
tags: [about, bio]
draft: false
summary: "Bio of the author of this site"
---
{{<figure
    src="/images/photos/photo.jpg"
    alt="qrcode representing site web address"
    class="right"
    height="350px"
    caption="Me using my old Nikon D60 &copy;"
>}}

```python3
>>> print('Hello, world!')
```

Hi visitor, my name is Riccardo Macoratti, an Italian young man, that also happens to be the owner of this site.

Just to know each other better, here are some quick information about myself.

I was born at the end of 1992 in [Rho](https://www.openstreetmap.org/relation/45188), a relatively big center near Milan. I lived most of early life divided between my home town, [Somma Lombardo](https://www.openstreetmap.org/relation/45572), in Lombardia, and [Gonars](https://www.openstreetmap.org/relation/179250), in Friuli Venezia Giulia.

Due to work reasons, I recently moved to [Viggiù](https://www.openstreetmap.org/relation/46319).

# {{<icon "fa fa-graduation-cap">}} Studies

My high school studies were mostly in modern languages, namely English, Spanish and German. I attended the [I.S.I.S Alessandro Manzoni](http://www.liceimanzoni.it/).
I am quite a fluent English spearker, also remembering quite well the Spanish language. Unfortunately, this cannot be said for my German skill, which, at the moment, are pretty low.

Since I have always been a tehcnology enthusiast and consider myself a geek, at 19 I changed my career to be more coherent to my interests and thus closer to technology.

I started attending a Bacherlor's degree course in [Digital Communication](http://www.ccdinfmi.unimi.it/it/corsiDiStudio/2015/F2Xof1/manifesto.html) at the [Università degli Studi di Milano](http://www.unimi.it/en). I graduated in December 2015 with a mark of 110/110, submitting a dissertation called "*Design and creation of a digital image capturing system and streaming towards a distributed player*" that can be viewed [here](/files/uni/tesi/tesi.pdf).

In January 2015, I started a Master's degree course in [Computer Science](http://www.ccdinfmi.unimi.it/it/corsiDiStudio/2017/F94of2/manifesto.html) again at the [Università degli Studi di Milano](http://www.unimi.it/en). I graduated in April 2018 with a mark of 109/110. My dissertation was called "*An application for remote competitions system: analysis, revision and implementation of new features*". It can be obtained [here](/files/uni/tesi/tesi-magistrale.pdf).

The second dissertation lead to a pubblication that was accepted at the [CSEDU 2018](http://www.csedu.org/?y=2018) conference. The recap poster of the paper won the *Best Poster Award* prize. Here the digital copies of the [paper](/files/uni/tesi/csedu2018.pdf) and its [poster](/files/uni/tesi/csedu2018-poster.pdf).

# {{<icon "fa fa-heart">}} Interests

Generally speaking, I consider myself a **geek**, genuinely fascinated by technology itself and its limits.
Since my early times, I began to tinker with my computer, automating repetitive tasks, producing scripts to achieve particular configurations. Basically I wanted to know more about computers, how they were built and how they operated. Hence my study and work career.

I really and deeply love the open source world, beacuse it opened my eyes to my passion.
My appreciation is directed especially towards GNU/Linux, by far my operating system of choice.

{{<figure
    src="/images/pages/neo-tux.png"
    alt="tux, the linux logo, image, dressed as neo in matrix"
    class="left"
    height="200px"
    caption="I always **loved** this portrait of Tux"
>}}

In 2014 I started attending a small LUG (Linux User Group) called
[LinuxVar](http://www.linuxvar.it). We are actually more focused on
spreading the GNU/Linux word, but sometimes we work on some little
projects, helping each other and sharing knowledge.

Once in a while we give talks and lessons to high school students, not
only on GNU/Linux, but more in general on open source technologies and
computer science.

Lastly, every so often, we organize themed nights to speak about an
interesting open source matter or application, where whoever feels more
condifent may arrange a small presentation on the subject, thus sharing
the knowledge.

Nowadays, due to my full-time job and other life matters, I play a little less with my computer. Nonetheless, every once in a while, I don't disdain to spend some time on little programming projects. If and when there is any development, it can be seen on my [GitLab](https://gitlab.com/ricmaco) profile.

I also have other hobbies not realted to computers, such as martial arts and DIY.
I also a very greedy YouTube watcher.

I really hope the content I write here will bring some enjoyment. To contact me, feel free to refer to one of the links in the homepage.