[![Pipeline status](https://gitlab.com/ricmaco/ricmaco.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/ricmaco/ricmaco.gitlab.io/commits/master)
[![Netlify Status](https://api.netlify.com/api/v1/badges/72dc28db-3d71-4d9b-a4ef-42bbbfbe92be/deploy-status)](https://app.netlify.com/sites/ricmaco/deploys)

# RicMa.co

Source code for the [RicMa.co](https://ricma.co) site.

I built it using the excellent [Hugo](https://gohugo.io/), a very fast static site generator written in Go.

## Build

Well, it has never been so simple. Just:

```bash
$ hugo
```

And the site gets built in the `public` directory.

## Display

To develop on this site just:

```bash
$ hugo server
```

Then head your favorite Web browser to http://localhost:1313/.
